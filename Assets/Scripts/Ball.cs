﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Ball : MonoBehaviour
{
    public Vector2 ballPosition;
    public Vector2 ballVelocity;
    public Vector2 ballMax;
 
    // Update is called once per frame
    void Update()
    {
        ballPosition.y = transform.position.y + ballVelocity.y*Time.deltaTime;
 
        if(ballPosition.y>ballMax.y){
            ballPosition.y = ballMax.y;
            ballVelocity.y*=-1;
        }else if(ballPosition.y<-ballMax.y){
            ballPosition.y = -ballMax.y;
            ballVelocity.y*=-1;
        }
 
        ballPosition.x = transform.position.x + ballVelocity.x*Time.deltaTime;
 
        if(ballPosition.x>ballMax.x){
            ballPosition.x= ballMax.x;
            ballVelocity.x*=-1;
        }else if(ballPosition.x<-ballMax.x){
            ballPosition.x = -ballMax.x;
            ballVelocity.x*=-1;
        }
 
        transform.position = new Vector3(ballPosition.x, ballPosition.y, transform.position.z);    
    }
}