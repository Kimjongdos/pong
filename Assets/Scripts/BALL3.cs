﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BALL3 : MonoBehaviour
{
    public Vector2 velocity;
    private Rigidbody2D rigid;
    // Start is called before the first frame update
    void Start()
    {
        rigid= GetComponent<Rigidbody2D>();
        rigid.AddForce(velocity, ForceMode2D.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
